variable "prefix" {
  default = "recipe-app-api-devops"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "k.devops.playground@gmail.com"
}