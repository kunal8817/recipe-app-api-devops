terraform {
  backend "s3" {
    bucket = "recipe-app-api-k.devops-tfstate"
    key    = "recipe-app-tfstate"
    region = "us-east-1"
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
